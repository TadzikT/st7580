package sample;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {
    private Controller controller = new Controller();
    private Button showPortListButton = new Button();
    private Button writeAndReadData = new Button();
    private Label label = new Label("Wyślij konfigurację: ");
    private ToggleGroup configGroup = new ToggleGroup();
    private RadioButton firstRadio = new RadioButton("0x10");
    private RadioButton secondRadio = new RadioButton("0x11");
    private ListView<String> portlist = new ListView<>();
    private ObservableList<String> portListItems = FXCollections.observableArrayList("");

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("ST7580 Swift Controller");
        Scene scene = new Scene(new Group(), 300, 300);
        VBox vbox = new VBox();
        vbox.setLayoutX(20);
        vbox.setLayoutY(20);

        showPortListButton.setText("Pokaż listę portów");
        showPortListButton.setOnAction(event -> {
            portListItems = FXCollections.observableArrayList(controller.showPortList());
            portlist.setItems(portListItems);
        });
        portlist.setPrefWidth(100);
        portlist.setPrefHeight(70);
        portlist.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> {
                    writeAndReadData.setDisable(false);
                    firstRadio.setDisable(false);
                    secondRadio.setDisable(false);
        });

        writeAndReadData.setText("Wyślij reset");
        writeAndReadData.setDisable(true);
        writeAndReadData.setOnAction(event ->
                controller.writeAndReadData(portlist.getSelectionModel().getSelectedItem(), 0));

        label.setPadding(new Insets(10, 0, 0, 0));
        firstRadio.setDisable(true);
        firstRadio.setToggleGroup(configGroup);
        firstRadio.setOnAction(event ->
            controller.writeAndReadData(portlist.getSelectionModel().getSelectedItem(), 1));
        secondRadio.setDisable(true);
        secondRadio.setToggleGroup(configGroup);
        secondRadio.setOnAction(event ->
            controller.writeAndReadData(portlist.getSelectionModel().getSelectedItem(), 2));
        secondRadio.setSelected(true);

        vbox.getChildren().addAll(showPortListButton, portlist, writeAndReadData, label, firstRadio, secondRadio);
        ((Group) scene.getRoot()).getChildren().add(vbox);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}