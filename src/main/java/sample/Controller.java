package sample;

import jssc.*;

import java.util.ArrayDeque;

public class Controller {
    private static SerialPort serialPort;
    private SerialPortReader serialPortReader = new SerialPortReader();
    private byte[] bytes;
    ArrayDeque<Byte> data = new ArrayDeque<>();

    public String[] showPortList() {
        return SerialPortList.getPortNames();
    }

    public void writeAndReadData(String portName, int command) { // 0 = reset, 1 = change config to 0x10, 2 = change config to 0x11
        serialPort = new SerialPort(portName);
        try {
            serialPort.openPort();
            serialPort.setParams(SerialPort.BAUDRATE_57600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);
            serialPort.setFlowControlMode(1);
            serialPort.setEventsMask(SerialPort.MASK_RXCHAR + SerialPort.MASK_CTS + SerialPort.MASK_DSR);
            serialPort.addEventListener(serialPortReader);

            serialPort.setRTS(true);
            serialPort.setDTR(true);

            for (int i = 0; i < 1000; i++) {
                bytes = serialPort.readBytes();
                if (bytes != null) {
                    System.out.println("sygnal: ");
                    printBytes(bytes);
                    for (byte bbyte : bytes) {
                        data.addLast(bbyte);
                    }
                    while (!data.isEmpty()) {
                        serialPortReader.tryReadingFrame();
                    }
                }
            }

            if (command == 0) {
                serialPort.writeBytes(BIO_ResetRequestFrame());
            } else if (command == 1) {
                serialPort.writeBytes(MIB_WriteRequestFrame(true));
            } else if (command == 2) {
                serialPort.writeBytes(MIB_WriteRequestFrame(false));
            }

            serialPort.setRTS(false);
            serialPort.setDTR(true);

            serialPort.closePort();
            System.out.println("queue elements: ");
            System.out.println(data);
            System.out.println("########################");
        } catch (SerialPortException  ex) {
            ex.printStackTrace();
        }
    }

    private byte[] BIO_ResetRequestFrame() {
        byte stx = 0x02;
        byte length = 0x00;
        byte BIO_ResetConfirm = 0x3C;
        byte checksum1 = 0x3C;
        byte checksum2 = 0x00;
        return new byte[] {stx, length, BIO_ResetConfirm, checksum1, checksum2};
    }

    private byte[] MIB_WriteRequestFrame(boolean command) { // true = 0x10, false = 0x11
        byte stx = 0x02;
        byte length = 0x02;
        byte MIB_WriteRequest = 0x08;
        byte data1 = 0x00; // index of MIB Table
        byte data2 = (byte)(command ? 0x10 : 0x11); // data to change value to
        byte checksum1 = (byte)(command ? 0x1A : 0x1B);
        byte checksum2 = 0x00;
        return new byte[] {stx, length, MIB_WriteRequest, data1, data2, checksum1, checksum2};
    }

    class SerialPortReader implements SerialPortEventListener {

        public void serialEvent(SerialPortEvent event) {
            if (event.isRXCHAR()) {
                try {
                    System.out.println("\nsygnal RXCHAR: ");
                    byte[] bytes = serialPort.readBytes(event.getEventValue());
                    Controller.printBytes(bytes);
                    for (byte bbyte : bytes) {
                        data.addLast(bbyte);
                    }
                    while (!data.isEmpty()) {
                        tryReadingFrame();
                    }
                } catch (SerialPortException ex) {
                    System.out.println(ex);
                }
            }
            else if (event.isCTS()) {
                if (event.getEventValue() == 1) {
                    System.out.println("CTS - ON");
                }
                else {
                    System.out.println("CTS - OFF");
                }
            }
            else if (event.isDSR()) {
                if (event.getEventValue() == 1) {
                    System.out.println("DSR - ON");
                }
                else {
                    System.out.println("DSR - OFF");
                }
            }
        }

        public void tryReadingFrame() {
            System.out.println("reading frame: ");
            byte stx = data.removeFirst();
            printBytes(new byte[] { stx });
            if (stx == 0x02 || stx == 0x03) {
                System.out.println("STX field: ");
                printBytes(new byte[]{stx});
            } else if (stx == 0x06) {
                System.out.println("!!! ACK");
                System.out.println("reading frame ended");
                return;
            } else if (stx == 0x15) {
                System.out.println("!!! NAK");
                System.out.println("reading frame ended");
                return;
            } else if (stx == 0x3F) {
                System.out.println("!!! Status message");
            } else {
                System.out.println("reading frame ended");
                return;
            }
            System.out.println("length field: ");
            byte lengthByte = data.removeFirst();
            if (stx == 0x3F) {
                System.out.println("Status second byte: ");
                printBytes(new byte[] { lengthByte });
                System.out.println("reading frame ended");
                return;
            }
            int length = lengthByte;
            System.out.println("length int: " + length);
            System.out.println("command code field: ");
            byte commandCodeField = data.removeFirst();
            printBytes(new byte[] { commandCodeField });
            if (commandCodeField == 0x3E) {
                System.out.println("!!! BIO_ResetIndication");
            } else if (commandCodeField == 0x3D) {
                System.out.println("!!! BIO_ResetConfirm");
            } else if (commandCodeField == 0x09) {
                System.out.println("!!! MIB_WriteConfirm");
            }
            System.out.println("data field: ");
            for (int i = 0; i < length; i++) {
                printBytes(new byte[]{ data.removeFirst() });
            }
            System.out.println("checksum field: ");
            printBytes(new byte[]{ data.removeFirst(), data.removeFirst() });
            System.out.println("reading frame ended");
        }
    }

    public static void printBytes(byte[] bytes) {
        for (byte byteToPrint: bytes) {
            System.out.printf("0x%02X ", byteToPrint);
        }
        System.out.print("\n");
    }

}
